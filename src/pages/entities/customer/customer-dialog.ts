import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { IonicPage, NavController, ViewController } from 'ionic-angular';

@IonicPage()
@Component({
    selector: 'page-customer-dialog',
    templateUrl: 'customer-dialog.html'
})
export class CustomerDialogPage {
    //@ViewChild('fileInput') fileInput;

    isReadyToSave: boolean;

    customer: any;

    form: FormGroup;

    constructor(public navCtrl: NavController, public viewCtrl: ViewController, formBuilder: FormBuilder) {
        this.form = formBuilder.group({
            id: [''],
            senderFio: ['', ],
            receiverFio: ['', ],
            cellPhone: ['', ],
            homePhone: ['', ]
        });

        // Watch the form for changes, and
        this.form.valueChanges.subscribe((v) => {
            this.isReadyToSave = this.form.valid;
        });
    }

    ionViewDidLoad() {
    }

    /**
     * The user cancelled, dismiss without sending data back.
     */
    cancel() {
        this.viewCtrl.dismiss();
    }

    /**
     * The user is done and wants to create the customer, so return it
     * back to the presenter.
     */
    done() {
        if (!this.form.valid) { return; }
        this.viewCtrl.dismiss(this.form.value);
    }
}
