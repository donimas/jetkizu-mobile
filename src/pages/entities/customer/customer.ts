import { Component } from '@angular/core';
import { IonicPage, ModalController, NavController, ToastController } from 'ionic-angular';
import { Customer } from './customer.model';
import { CustomerService } from './customer.provider';

@IonicPage()
@Component({
    selector: 'page-customer',
    templateUrl: 'customer.html'
})
export class CustomerPage {
    customers: Customer[];

    // todo: add pagination

    constructor(private navCtrl: NavController, private customerService: CustomerService,
                private modalCtrl: ModalController, private toastCtrl: ToastController) {
        this.customers = [];
    }

    ionViewDidLoad() {
        this.loadAll();
    }

    loadAll() {
        this.customerService.query().subscribe(
            (response) => this.onSuccess(response),
            (error) => this.onError(error));
    }

    private onSuccess(data) {
        this.customers = data;
    }

    private onError(error) {
        console.error(error);
        // todo: use toaster, this.jhiAlertService.error(error.message, null, null);
    }

    trackId(index: number, item: Customer) {
        return item.id;
    }

    add() {
        let addModal = this.modalCtrl.create('CustomerDialogPage');
        addModal.onDidDismiss(customer => {
            if (customer) {
                this.customerService.create(customer).subscribe(data => {
                    this.customers.push(data);
                    let toast = this.toastCtrl.create({
                        message: 'Customer added successfully.',
                        duration: 3000,
                        position: 'top'
                    });
                    toast.present();
                }, (error) => console.error(error));
            }
        });
        addModal.present();
    }

    delete(customer) {
        this.customerService.delete(customer.id).subscribe(() => {
            let toast = this.toastCtrl.create({
                message: 'Customer deleted successfully.',
                duration: 3000,
                position: 'top',
            });
            toast.present();
            this.loadAll();
        }, (error) => console.error(error));
    }

    open(customer: Customer) {
        this.navCtrl.push('CustomerDetailPage', {
            customer: customer
        });
    }
}
