import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { CustomerPage } from './customer';
import { CustomerService } from './customer.provider';

@NgModule({
    declarations: [
        CustomerPage
    ],
    imports: [
        IonicPageModule.forChild(CustomerPage),
        TranslateModule.forChild()
    ],
    exports: [
        CustomerPage
    ],
    providers: [CustomerService]
})
export class CustomerPageModule {
}
