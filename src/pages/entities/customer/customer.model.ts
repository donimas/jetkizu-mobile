import { BaseEntity } from './../../../models';

export class Customer implements BaseEntity {
    constructor(
        public id?: number,
        public senderFio?: string,
        public receiverFio?: string,
        public cellPhone?: string,
        public homePhone?: string,
    ) {
    }
}
