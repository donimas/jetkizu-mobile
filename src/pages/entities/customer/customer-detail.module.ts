import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { CustomerDetailPage } from './customer-detail';
import { CustomerService } from './customer.provider';

@NgModule({
    declarations: [
        CustomerDetailPage
    ],
    imports: [
        IonicPageModule.forChild(CustomerDetailPage),
        TranslateModule.forChild()
    ],
    exports: [
        CustomerDetailPage
    ],
    providers: [CustomerService]
})
export class CustomerDetailPageModule {
}
