import { Component } from '@angular/core';
import { IonicPage, NavParams } from 'ionic-angular';
import { Customer } from './customer.model';
import { CustomerService } from './customer.provider';

@IonicPage()
@Component({
    selector: 'page-customer-detail',
    templateUrl: 'customer-detail.html'
})
export class CustomerDetailPage {
    customer: Customer;

    constructor(navParams: NavParams) {
        this.customer = navParams.get('customer');
    }
}
