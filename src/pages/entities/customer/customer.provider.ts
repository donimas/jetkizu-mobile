import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import { Api } from '../../../providers/api/api';

import { Customer } from './customer.model';

@Injectable()
export class CustomerService {
    private resourceUrl = Api.API_URL + '/customers';

    constructor(private http: HttpClient) { }

    create(customer: Customer): Observable<Customer> {
        return this.http.post(this.resourceUrl, customer);
    }

    update(customer: Customer): Observable<Customer> {
        return this.http.put(this.resourceUrl, customer);
    }

    find(id: number): Observable<Customer> {
        return this.http.get(`${this.resourceUrl}/${id}`);
    }

    query(req?: any): Observable<any> {
        return this.http.get(this.resourceUrl);
    }

    delete(id: number): Observable<any> {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response', responseType: 'text' });
    }
}
