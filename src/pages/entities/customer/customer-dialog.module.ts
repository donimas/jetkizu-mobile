import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { CustomerDialogPage } from './customer-dialog';
import { CustomerService } from './customer.provider';

@NgModule({
    declarations: [
        CustomerDialogPage
    ],
    imports: [
        IonicPageModule.forChild(CustomerDialogPage),
        TranslateModule.forChild()
    ],
    exports: [
        CustomerDialogPage
    ],
    providers: [CustomerService]
})
export class CustomerDialogPageModule {
}
